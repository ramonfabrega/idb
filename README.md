Name: Ramon Fabrega
EID: RF23826
Gitlab ID: ramonfabrega
Estimated time: 2 hours
Actual time: 3 hours

Name: Celik Savk
EID: cs56865
Gitlab ID: csavk
Estimated time: 4 hours
Actual time: 2 hours

Name: Teo Jun Hui
EID: tjh3364
Gitlab ID: junhui096
Estimated : 1 hours
Actual: 1 hours

Name: David Neveling
EID: dan798
Gitlab ID: DavidNeveling
Estimated Time: 4 hours
Actual Time: 4 hours

Name: Anthony Moeller
EID: Ajm6396
Gitlab ID: SuspectTax
Estimated: 7 hours
Actual: 10 hours

Name: Supawit Chockchowwat
EID: sc54552
Gitlab ID: mixs222
Estimated Time: 3 hours
Actual Time: 3 hours

Git SHA: 2c46308de6ee0b6f76b557f2b7a80912be34b787
Postman API: https://documenter.getpostman.com/view/5497939/RWgm2LED
